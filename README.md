# gyst - get your songs together

A tool which let's you quickly download huge ammounts of songs at once. Paste all the songs into the app window and click download.

gyst is distributed without any warranty.

## System requirements

- Windows Vista SP2 or newer
- .NET Framework 4.6 installed

## Used third-party technologies

- [FFMpeg](https://www.ffmpeg.org) - [License](https://www.ffmpeg.org/legal.html)
- .NET Framework 4.6
- [youtube-dl](https://ytdl-org.github.io/youtube-dl/)

## License

This program is distributed under GNU General Public License v3. Feel free to use its source code as long as you mention the author and state the changes. You can also modify, share and distribute it, as long as you state changes. For more, see [LICENSE](LICENSE.md) file.

## Versioning

We're version this project with sequence-based identifiers versioning (1.0, then 1.1...).

## Building the source code

The program was build with Microsoft Visual Studio 2019 Community, so I think it's the best way to compile it.

## Author(s)

- [Athlon](http://twitter.com/_athl/)

## Special thanks

Special thanks to Apple Inc. and their shitty iTunes for deleting music from my mom's iPhone and forcing me to get all the music again.