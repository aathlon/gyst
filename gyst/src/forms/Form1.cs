﻿// gyst
// Copyright(C) 2019 Athlon

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.If not, see<http://www.gnu.org/licenses/>.

using System;
using System.Windows.Forms;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace gyst
{
    public partial class Form1 : Form
    {
        public static Form1 instance;

        public Form1()
        {
            InitializeComponent();
            instance = this;
        }

        /// <summary>
        /// Writes value to log output
        /// </summary>
        /// <param name="value">Value to output</param>
        public void Log(string value)
        {
            if (value == "")
                return;

            value = value.Replace("\n", Environment.NewLine);
            value += Environment.NewLine;

            if (log.InvokeRequired)
            {
                log.Invoke(new Action(delegate ()
                {
                    log.Text += value;
                    log.SelectionStart = log.TextLength;
                    log.ScrollToCaret();
                }));
                return;
            }

            log.Text += value;
            log.SelectionStart = log.TextLength;
            log.ScrollToCaret();
        }

        private async void BtnStartDownload_Click(object sender, EventArgs e)
        {
            songsList.Text = Regex.Replace(songsList.Text, @"^\s*$\n|\r", string.Empty, RegexOptions.Multiline).TrimEnd();
            ListManager.Backup(songsList.Text);

            string[] songs = songsList.Text.Split('\n');
            barProgress.Value = 0;
            labProgress.Text = $"0/{songs.Length} (0%)";

            for (int i = 0; i < songs.Length; i++)
            {
                if (songs[i] == "") continue;
                await Downloader.DownloadFile(songs[i]);

                songsList.Text = songsList.Text.Replace(songs[i], "");
                songsList.Text = Regex.Replace(songsList.Text, @"^\s*$\n|\r", string.Empty, RegexOptions.Multiline).TrimEnd();
                double percentage = (double)(i + 1) / (double)songs.Length * (double)100;
                labProgress.Text = $"{i + 1}/{songs.Length} ({Math.Round(percentage)}%)";
                barProgress.Value = (int)percentage;
                ListManager.Save(songsList.Text);
            }

            Log("Finished");
        }

        private void BtnClip_Click(object sender, EventArgs e)
        {
            songsList.Text = songsList.Text + Clipboard.GetText() + Environment.NewLine;
            songsList.SelectionStart = songsList.TextLength;
            songsList.ScrollToCaret();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            File.WriteAllText("dump.txt", songsList.Text);
            ListManager.Save(songsList.Text);
        }

        private void BtnLoad_Click(object sender, EventArgs e)
        {
            if (ModifierKeys.HasFlag(Keys.Shift))
            {
                songsList.Text = ListManager.LoadFromBackup();
                return;
            }

           songsList.Text = ListManager.Load();
        }

        private void BtnClearLog_Click(object sender, EventArgs e)
        {
            log.Text = "";
        }

        private void Form1_Deactivate(object sender, EventArgs e)
        {
            ListManager.Save(songsList.Text);
        }
    }
}
