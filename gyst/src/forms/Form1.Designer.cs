﻿namespace gyst
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.songsList = new System.Windows.Forms.TextBox();
            this.log = new System.Windows.Forms.TextBox();
            this.btnStartDownload = new System.Windows.Forms.Button();
            this.btnClip = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnLoad = new System.Windows.Forms.Button();
            this.btnClearLog = new System.Windows.Forms.Button();
            this.barProgress = new System.Windows.Forms.ProgressBar();
            this.labProgress = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // songsList
            // 
            this.songsList.Location = new System.Drawing.Point(12, 39);
            this.songsList.Multiline = true;
            this.songsList.Name = "songsList";
            this.songsList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.songsList.Size = new System.Drawing.Size(384, 379);
            this.songsList.TabIndex = 0;
            // 
            // log
            // 
            this.log.Location = new System.Drawing.Point(404, 39);
            this.log.Multiline = true;
            this.log.Name = "log";
            this.log.ReadOnly = true;
            this.log.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.log.Size = new System.Drawing.Size(384, 379);
            this.log.TabIndex = 1;
            // 
            // btnStartDownload
            // 
            this.btnStartDownload.Location = new System.Drawing.Point(681, 10);
            this.btnStartDownload.Name = "btnStartDownload";
            this.btnStartDownload.Size = new System.Drawing.Size(107, 23);
            this.btnStartDownload.TabIndex = 2;
            this.btnStartDownload.Text = "Start Download";
            this.btnStartDownload.UseVisualStyleBackColor = true;
            this.btnStartDownload.Click += new System.EventHandler(this.BtnStartDownload_Click);
            // 
            // btnClip
            // 
            this.btnClip.Location = new System.Drawing.Point(12, 10);
            this.btnClip.Name = "btnClip";
            this.btnClip.Size = new System.Drawing.Size(141, 23);
            this.btnClip.TabIndex = 3;
            this.btnClip.Text = "Add from clipboard";
            this.btnClip.UseVisualStyleBackColor = true;
            this.btnClip.Click += new System.EventHandler(this.BtnClip_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 428);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Pre-Alpha 0.1";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(268, 10);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(61, 23);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(335, 10);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(61, 23);
            this.btnLoad.TabIndex = 6;
            this.btnLoad.Text = "Load";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.BtnLoad_Click);
            // 
            // btnClearLog
            // 
            this.btnClearLog.Location = new System.Drawing.Point(404, 10);
            this.btnClearLog.Name = "btnClearLog";
            this.btnClearLog.Size = new System.Drawing.Size(90, 23);
            this.btnClearLog.TabIndex = 7;
            this.btnClearLog.Text = "Clear";
            this.btnClearLog.UseVisualStyleBackColor = true;
            this.btnClearLog.Click += new System.EventHandler(this.BtnClearLog_Click);
            // 
            // barProgress
            // 
            this.barProgress.Location = new System.Drawing.Point(305, 424);
            this.barProgress.Name = "barProgress";
            this.barProgress.Size = new System.Drawing.Size(483, 23);
            this.barProgress.TabIndex = 8;
            // 
            // labProgress
            // 
            this.labProgress.AutoSize = true;
            this.labProgress.Location = new System.Drawing.Point(232, 428);
            this.labProgress.Name = "labProgress";
            this.labProgress.Size = new System.Drawing.Size(47, 13);
            this.labProgress.TabIndex = 9;
            this.labProgress.Text = "0/0 (0%)";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.labProgress);
            this.Controls.Add(this.barProgress);
            this.Controls.Add(this.btnClearLog);
            this.Controls.Add(this.btnLoad);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnClip);
            this.Controls.Add(this.btnStartDownload);
            this.Controls.Add(this.log);
            this.Controls.Add(this.songsList);
            this.Name = "Form1";
            this.Text = "gyst";
            this.Deactivate += new System.EventHandler(this.Form1_Deactivate);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox songsList;
        private System.Windows.Forms.TextBox log;
        private System.Windows.Forms.Button btnStartDownload;
        private System.Windows.Forms.Button btnClip;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.Button btnClearLog;
        private System.Windows.Forms.ProgressBar barProgress;
        private System.Windows.Forms.Label labProgress;
    }
}

