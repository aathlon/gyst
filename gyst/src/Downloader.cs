﻿// gyst
// Copyright(C) 2019 Athlon

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.If not, see<http://www.gnu.org/licenses/>.

using System;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;

namespace gyst
{
    class Downloader
    {
        public static bool IsBusy { get; set; }

        /// <summary>
        /// Downloads the song as .ACC file
        /// </summary>
        /// <param name="url">URL link to video</param>
        /// <returns></returns>
        public static async Task DownloadFile(string url)
        {
            try
            {
                if (!url.ContainsAny("https://www.youtube.com/watch?v=", "https://youtube.com/watch?v=", "ytsearch:"))
                {
                    Form1.instance.Log("Not a valid URL. Currently only YouTube is suported.");
                    return;
                }

                IsBusy = true;

                Form1.instance.Log($"Downloading \"{url}\"");

                Directory.CreateDirectory("songs");

                Process process = new Process();
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.CreateNoWindow = true;

                // Setup executable and parameters
                process.StartInfo.FileName = "youtube-dl.exe";
                process.StartInfo.Arguments = $"-f bestaudio -x --audio-format mp3 --audio-quality 0 -o \"songs\\%(title)s.%(ext)s\" {url}";
                process.Start();
                await Task.Run(() => process.WaitForExit());

                Form1.instance.Log("Done!");

                IsBusy = false;
            }
            catch (Exception ex)
            {
                Form1.instance.Log("Couldn't get " + url + "\n\n" + ex.ToString());
            }
        }
    }
}
