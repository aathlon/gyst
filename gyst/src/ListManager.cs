﻿// gyst
// Copyright(C) 2019 Athlon

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.If not, see<http://www.gnu.org/licenses/>.

using System.IO;

namespace gyst
{
    class ListManager
    {
        public static void Save(string value)
        {
            File.WriteAllText("dump.txt", value);
        }

        public static string Load()
        {
            return File.ReadAllText("dump.txt");
        }

        public static void Backup(string value)
        {
            File.WriteAllText("backup.txt", value);
        }

        public static string LoadFromBackup()
        {
            return File.ReadAllText("backup.txt");
        }
    }
}
